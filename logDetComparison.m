% CAMSAP 2017
% Log-det Optimization
% Mario Coutino
clear
close all

%% Set up environment

fprintf('Creating setup...\n\n');
% number of sensors
n = 15;
V = 1:n;

if n <= 15
    hstp = 1;
else
    hstp = 5;
end

% generation of sampling matrix
E = eye(n);
Iw = @(A) E(:,A)*E(:,A)';

p = 3;
q = 2;
% minimum number of sensors
pq = p + q;

% creation of subspaces
angles = [-30 0 50];
% angles = rand(1,p)*180 - 90;
jammer = [-70 30];
% jammer = rand(1,q)*180 - 90;
H = nan(n,p);
S = nan(n,q);
for pp = 1:p
    H(:,pp) = gen_a(n,0.5,angles(pp));
end
for qq = 1:q
    S(:,qq) = gen_a(n,0.5,jammer(qq));
end

[Us,~,~] = svd(S);
Ps_perp = Us(:,q+1:end)*Us(:,q+1:end)';
Pbar = rank(Ps_perp*H);

%% Exhaustive Search

% logDet function
flog = @(A) logdet(H'*Iw(A)*H - H'*Iw(A)*S*(S'*Iw(A)*S)^-1*S'*Iw(A)*H); 
flog_wst = @(A) -logdet(H'*Iw(A)*H - H'*Iw(A)*S*(S'*Iw(A)*S)^-1*S'*Iw(A)*H); 

Flog = sfo_fn_wrapper(flog);
Flog_wst = sfo_fn_wrapper(flog_wst);

if n <= 15
    fprintf('Exhaustive Search logDet...\n\n');
    valExh = zeros(n,1);
    valWst = zeros(n,1);
    for nn = pq:n
        Aexh = sfo_exhaustive_max(Flog,V,nn);
        valExh(nn) = Flog(Aexh');
        Awst = sfo_exhaustive_max(Flog_wst,V,nn);
        valWst(nn) = Flog(Awst');
    end
    valWst(valWst < 0) = 0;
end

%% Submodular Function
fprintf('Submodular Surrogates...\n\n')
fRlx = @(A) logdet([S'*Iw(A)*S S'*Iw(A)*H;...
                    H'*Iw(A)*S H'*Iw(A)*H]);

Frlx = sfo_fn_wrapper(fRlx);

fprintf('Submodular logDet Maximization...\n\n')
Asub = sfo_greedy_k(Flog,V,n);
valSub = kSetFunctionCost(Flog,Asub');
Arlx = sfo_greedy_k(Frlx,V,n);
valRlx = kSetFunctionCost(Flog,Arlx');

%% Convex Formulation
valLogDetCVX = zeros(n,1);
% parameters for reweighted l1-norm
epsilon = 1e-3;
maxIt = 10;
gamma = 10;
fprintf('CVX logDet...\n\n');
for nn = pq:hstp:n
    u = ones(n,1);
    for ll = 1:maxIt
        cvx_begin sdp quiet
            variable Z(p,p) symmetric
            variable w(n,1)
            maximize ( log_det(H'*diag(w)*H - Z) - gamma*sum(w./u))
            subject to
                [S'*diag(w)*S S'*diag(w)*H;
                    H'*diag(w)*S Z] >= 0;
                zeros(n,1) <= w <= ones(n,1);
                ones(1,n)*w == nn;
        cvx_end
        u = w + epsilon;
        wSol = w>epsilon;
%         fprintf('%d\n', nnz(wSol))
        if nnz(wSol) == nn
            valLogDetCVX(nn) = real(Flog(V(wSol)));
            break;
        else
            %when no nn-sparse solution is retrieved
            if nn > 1
                valLogDetCVX(nn) = valLogDetCVX(nn-1);
            end
        end
    end
end

%% Computation of PD
Qbar = n - q;
PFA = 1e-1;
eta = icdf('F',1 - PFA,Pbar,Qbar - Pbar);

if n <= 15
    Pd_exh = 1 - ncfcdf(eta,Pbar,Qbar-Pbar,valExh);
    Pd_wst = 1 - ncfcdf(eta,Pbar,Qbar-Pbar,valWst);
end
Pd_Sub = 1 - ncfcdf(eta,Pbar,Qbar-Pbar,valSub);
Pd_CVX_log = 1 - ncfcdf(eta,Pbar,Qbar-Pbar,valLogDetCVX);

Pd_rlx = 1 - ncfcdf(eta,Pbar,Qbar-Pbar,valRlx);

%% Plot Pd
close all
fprintf('Ploting...\n\n')
if n <= 15
    hfig = area(V(pq:end),[Pd_wst(pq:end)...
        Pd_exh(pq:end)-Pd_wst(pq:end) ...
    ],'LineStyle','none');
    hold on,    
    set(hfig(1),'FaceColor',[1 1 1])
    set(hfig(2),'FaceColor',[0.9 0.9 0.9])
    p1 = plot(V(pq:end), Pd_exh(pq:end),'-r'); hold on,
    pw = plot(V(pq:end), Pd_wst(pq:end),'-+r');
end
% p5 = plot(V(pq:end), Pd_Sub(pq:end),'-*b','linewidth',1.1); hold on,
p6 = plot(V(pq:hstp:end), Pd_CVX_log(pq:hstp:end),'-sc','linewidth',1.1);
p7 = plot(V(pq:end), Pd_rlx(pq:end),'-ob','linewidth',1.1);
if n > 15
    legend([p5;p6;p7],{'Greedy logDet','CVX logDet',...
        'Submodular logDet',...
        'CVX minEig','Submodular logDet'},...
        'location','southeast')
    
     legend([p6;p7],{'CVX logDet',...
        'Submodular logDet',...
        'CVX minEig','Submodular logDet'},...
        'location','southeast')
else
        legend([p1;p6;p7;pw;hfig(2)],{'Exhaustive Search',...
        'Greedy logDet','CVX logDet','Submodular logDet',...
        'Worst Case','PD of random samplers'},...
        'location','southeast')
    
        legend([p1;p6;p7;pw;hfig(2)],{'Exhaustive Search',...
        'CVX logDet','Submodular logDet',...
        'Worst Case','PD of random samplers'},...
        'location','southeast')
end
xlabel('Number of Measurements [k]')
ylabel('Probability of Detection [PD]')
set(gca,'fontsize',13)

%% Plot logDet
close all
fprintf('Ploting...\n\n')
if n <= 15
    hfig = area(V(pq:end),[valWst(pq:end)...
        valExh(pq:end)-valWst(pq:end) ...
    ],'LineStyle','none');
    hold on,    
    set(hfig(1),'FaceColor',[1 1 1])
    set(hfig(2),'FaceColor',[0.9 0.9 0.9])
    p1 = plot(V(pq:end), valExh(pq:end),'-r'); hold on,
    pw = plot(V(pq:end), valWst(pq:end),'-+r');
end
% p5 = plot(V(pq:end), Pd_Sub(pq:end),'-*b','linewidth',1.1); hold on,
p6 = plot(V(pq:hstp:end), valLogDetCVX(pq:hstp:end),'-sc','linewidth',1.1);
p7 = plot(V(pq:end), valRlx(pq:end),'-ob','linewidth',1.1);
if n > 15
    legend([p5;p6;p7],{'Greedy logDet','CVX logDet',...
        'Submodular logDet',...
        'CVX minEig','Submodular logDet'},...
        'location','southeast')
    
     legend([p6;p7],{'CVX logDet',...
        'Submodular logDet',...
        'CVX minEig','Submodular logDet'},...
        'location','southeast')
else
        legend([p1;p6;p7;pw;hfig(2)],{'Exhaustive Search',...
        'Greedy logDet','CVX logDet','Submodular logDet',...
        'Worst Case','PD of random samplers'},...
        'location','southeast')
    
        legend([p1;p6;p7;pw;hfig(2)],{'Exhaustive Search',...
        'CVX logDet','Submodular logDet',...
        'Worst Case','PD of random samplers'},...
        'location','southeast')
end
xlabel('Number of Measurements [k]')
ylabel('ln(det(G))')
set(gca,'fontsize',13)