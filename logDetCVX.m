n = 5;
H = randn(n,2);
S = randn(n,2);

cvx_begin sdp
    variable Z(2,2) symmetric
    variable w(n,1)
    maximize ( log_det(H'*diag(w)*H - Z) )
    subject to
    [S'*diag(w)*S S'*diag(w)*H;
        H'*diag(w)*S Z] >= 0;
    0 <= w <= 1
    
cvx_end