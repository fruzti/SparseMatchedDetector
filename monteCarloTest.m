% CAMSAP 2017
% Mario Coutino
clear all
close all
printEig = 1;
%% Set up environment

fprintf('Creating setup...\n\n');
% number of sensors
n = 100;
V = 1:n;

% generation of sampling matrix
E = eye(n);
Iw = @(A) E(:,A)*E(:,A)';

% generation of testing matrices
% A = randn(n,n);
% A = A*A';
% 
% L = sqrtm(A);

p = 3;
q = 2;
% pq = max(p,q);
pq = p + q;

% selection of subspaces
% H = L(:,1:p);
% S = L(:,p+1:p+q);

% angles = [-30 0 50];
angles = rand(1,p)*180 - 90;
% jammer = [-70 30];
jammer = rand(1,q)*180 - 90;
H = nan(n,p);
S = nan(n,q);
for pp = 1:p
    H(:,pp) = gen_a(n,0.5,angles(pp));
end
for qq = 1:q
    S(:,qq) = gen_a(n,0.5,jammer(qq));
end

[Us,~,~] = svd(S);
Ps_perp = Us(:,q+1:end)*Us(:,q+1:end)';
Pbar = rank(Ps_perp*H);

%% Greedy Optimization
fprintf('Greedy Optimization of minEig...\n\n');
% maximum minimum eigenvalue
lmin = min(eig(H'*H - H'*S*(S'*S)^-1*S'*H));

% min eig function
f = @(A) abs(min(eig(H'*Iw(A)*H - H'*Iw(A)*S*(S'*Iw(A)*S + 0.1*eye(q))^-1*S'*Iw(A)*H)));
ftrue = @(A) abs(min(eig(max(0,H'*Iw(A)*H - H'*Iw(A)*S*(S'*Iw(A)*S)^-1*S'*Iw(A)*H))));
fworst = @(A) -abs(min(eig(max(0,H'*Iw(A)*H - H'*Iw(A)*S*(S'*Iw(A)*S)^-1*S'*Iw(A)*H))));

Ftrue = sfo_fn_wrapper(ftrue);
Fworst = sfo_fn_wrapper(fworst);
F = sfo_fn_wrapper(f);
G = sfo_fn_invert(Ftrue,V);

if n <= 15
    Astar = sfo_exhaustive_max(Ftrue,V,pq);
    opt = sfo_opt({'greedy_initial_sset',Astar'});
end

fprintf('Greedy minEig...\n')
Agrd = sfo_greedy_k(F,V,n);
valGrd = kSetFunctionCost(Ftrue,Agrd');
valGrd(valGrd <= 0) = 0;

valGrdStar = zeros(n,1);
if n <= 15
    fprintf('Init Greedy minEig..\n')
    AgrdStar = sfo_greedy_k(Ftrue,V,n,opt);
    valGrdStar = kSetFunctionCost(Ftrue,AgrdStar');
    valGrdStar(valGrdStar < 0) = 0;
end

fprintf('Inv Greedy minEig...\n');
Ainv = flip(sfo_greedy_k(G,V,n-pq));
Ainv = [setdiff(V,Ainv) Ainv];

valInv = kSetFunctionCost(Ftrue,Ainv');
valInv(valInv <= 0) = 0;

%% Exhaustive Search
if n <= 15
    fprintf('Exhaustive Search minEig...\n\n');
    valExh = zeros(n,1);
    valWst = zeros(n,1);
    for nn = pq:n
        Aexh = sfo_exhaustive_max(F,V,nn);
        valExh(nn) = Ftrue(Aexh);
        Awst = sfo_exhaustive_max(Fworst,V,nn);
        valWst(nn) = Ftrue(Awst);
    end

end

%% Submodular Function
fprintf('Submodular Surrogates...\n\n')
fRlx = @(A) logdet([S'*Iw(A)*S S'*Iw(A)*H;...
                    H'*Iw(A)*S H'*Iw(A)*H]);

fSub = @(A) logdet(H'*Iw(A)*H - H'*Iw(A)*S*(S'*Iw(A)*S)^-1*S'*Iw(A)*H);                

Fsub = sfo_fn_wrapper(fSub);
Frlx = sfo_fn_wrapper(fRlx);

fprintf('Submodular logDet Maximization...\n\n')
Asub = sfo_greedy_k(Fsub,V,n);
valSub = kSetFunctionCost(Ftrue,Asub');
Arlx = sfo_greedy_k(Frlx,V,n);
valRlx = kSetFunctionCost(Ftrue,Arlx');

%% Convex MinEig Formulation
valCVX = zeros(n,1);
% parameters for reweighted l1-norm
epsilon = 1e-3;
maxIt = 10;
gamma = 10;
fprintf('CVX minEig...\n\n');
for nn = pq:5:n
    u = ones(n,1);
    for ll = 1:maxIt
        cvx_begin sdp quiet
            variable lambda_min
            variable w(n,1)
            maximize ( lambda_min - gamma*sum(w./u) )
            subject to
                [S'*diag(w)*S         S'*diag(w)*H;...
                H'*diag(w)*S   H'*diag(w)*H - lambda_min*eye(p)] >= 0;
                zeros(n,1) <= w <= ones(n,1);
                ones(1,n)*w == nn;
        cvx_end
        u = w + epsilon;
%         fprintf('l0-norm: %d\n',nnz(w>epsilon));
        wSol = w>epsilon;
        if nnz(wSol) == nn
            valCVX(nn) = Ftrue(V(wSol));
            break;
        else
            %when no nn-sparse solution is retrieved
            if nn > 1
                valCVX(nn) = valCVX(nn-1);
            end
        end
    end
end

%% Convex logDetCVX

valLogDetCVX = zeros(n,1);
% parameters for reweighted l1-norm
epsilon = 1e-3;
maxIt = 10;
gamma = 10;
fprintf('CVX logDet...\n\n');
for nn = pq:5:n
    u = ones(n,1);
    for ll = 1:maxIt
        cvx_begin sdp quiet
            variable Z(p,p) symmetric
            variable w(n,1)
            maximize ( log_det(H'*diag(w)*H - Z) - gamma*sum(w./u))
            subject to
                [S'*diag(w)*S S'*diag(w)*H;
                    H'*diag(w)*S Z] >= 0;
                zeros(n,1) <= w <= ones(n,1);
                ones(1,n)*w == nn;
        cvx_end
        u = w + epsilon;
        wSol = w>epsilon;
%         fprintf('%d\n', nnz(wSol))
        if nnz(wSol) == nn
            valLogDetCVX(nn) = real(Ftrue(V(wSol)));
            break;
        else
            %when no nn-sparse solution is retrieved
            if nn > 1
                valLogDetCVX(nn) = valLogDetCVX(nn-1);
            end
        end
    end
end

fprintf('Finished...\n\n');
%% Computation of PD

Qbar = n - q;
PFA = 1e-1;
eta = icdf('F',1 - PFA,Pbar,Qbar - Pbar);

Pd_Grd = 1 - ncfcdf(eta,Pbar,Qbar-Pbar,valGrd);
Pd_Inv = 1 - ncfcdf(eta,Pbar,Qbar-Pbar,valInv);
if n <= 15
    Pd_GrdStar = 1 - ncfcdf(eta,Pbar,Qbar-Pbar,valGrdStar);
    Pd_exh = 1 - ncfcdf(eta,Pbar,Qbar-Pbar,valExh);
    Pd_wst = 1 - ncfcdf(eta,Pbar,Qbar-Pbar,valWst);
end
Pd_Sub = 1 - ncfcdf(eta,Pbar,Qbar-Pbar,valSub);
Pd_CVX = 1 - ncfcdf(eta,Pbar,Qbar-Pbar,valCVX);
Pd_CVX_log = 1 - ncfcdf(eta,Pbar,Qbar-Pbar,valLogDetCVX);

Pd_rlx = 1 - ncfcdf(eta,Pbar,Qbar-Pbar,valRlx);

%% Plot Pd
fprintf('Ploting...\n\n')
if n <= 15

    hfig = area(V(pq:end),[Pd_wst(pq:end)...
        Pd_exh(pq:end)-Pd_wst(pq:end) ...
    ],'LineStyle','none');
    hold on,    
    set(hfig(1),'FaceColor',[1 1 1])
    set(hfig(2),'FaceColor',[0.9 0.9 0.9])
    p1 = plot(V(pq:end), Pd_exh(pq:end),'-r'); hold on,
    pw = plot(V(pq:end), Pd_wst(pq:end),'-+r');
    p4 = plot(V(pq:end), Pd_GrdStar(pq:end), '-ok','linewidth',1.1);
    
end
p2 = plot(V(pq:end), Pd_Grd(pq:end),'-sg','linewidth',1.1); hold on,
p3 = plot(V(pq:end), Pd_Inv(pq:end),'-dm','linewidth',1.1);
p5 = plot(V(pq:end), Pd_Sub(pq:end),'-*b','linewidth',1.1);
p6 = plot(V(pq:5:end), Pd_CVX(pq:5:end),'-sc','linewidth',1.1);
p7 = plot(V(pq:end), Pd_rlx(pq:end),'-ob','linewidth',1.1);
p8 = plot(V(pq:5:end), Pd_CVX_log(pq:5:end),'-dc','linewidth',1.1);
if n > 15
    legend([p2;p3;p5;p6;p7;p8],{'Fwd Greedy','Bck Greedy',...
        'Greedy logDet',...
        'CVX minEig','Submodular logDet', 'CVX logDet'},...
        'location','southeast')
else
        legend([p1;p2;p3;p4;p5;p6;p7;pw;hfig(2)],{'Exhaustive Search','Fwd Greedy',...
        'Bck Greedy','Init Fwd Greedy',...
        'Greedy logDet','CVX minEig','Submodular logDet',...
        'CVX logDet', 'Worst Case','PD of random samplers'},...
        'location','southeast')
end
xlabel('Number of Measurements [k]')
ylabel('Probability of Detection [PD]')
set(gca,'fontsize',13)