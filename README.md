Sparse Sensing for Composite Matched Subspace Detection

Mario Coutino - S.P Chepuri - G. Leus

This is the code for reproducing the figures in the paper "Sparse Sensing for Composite Matched Subspace Detection" presented in CAMSAP 2017.

It requires the CVX toolbox (http://cvxr.com/cvx/).
