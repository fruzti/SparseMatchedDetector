%% Test Compressing Detection (Singleton Subspace)
clear all, close all

% length of code
lCode = 1024;

% orthogonal codes
S = hadamard(lCode);
S = 1/norm(S(:,1)) * S;

% length of codeword <- reduced dimensionality
b = log2(lCode);
% b = 100;

% list of codewords
B = de2bi(0:lCode-1,b)';
B = B - ~B;

% random realisations
nTest = 1e3;

% noise
SNR = [-10:10:50];
lSNR = length(SNR);

% covariance matrix -> reduced dimensionality
C = B*S';
R0 = (C*C');

succ_prob = zeros(lSNR,1);
succ_theory = zeros(lSNR,1);

for ss = 1:lSNR
    sigma = sqrt(10^(-SNR(ss)/10));
    R = sigma^2 * R0;
    
    fprintf('SNR: %d dB\n\n', SNR(ss))
    for nn = 1:nTest
    
        ind_n = randi([1,lCode],1);
        x_n = S(:,ind_n) + sigma * randn(lCode,1);
    
        b_tst = B*S'*x_n;
    
%         b_dec = ( b_tst > 0 ) - ( b_tst < 0);
        b_dec = ( b_tst > 0 );
        
        ind_tst = bi2de(b_dec') + 1;
    
        succ_prob(ss) = succ_prob(ss) + ( ind_tst == ind_n )/nTest;
    end
    succ_theory(ss) = cdf('Normal',sqrt(1/(lCode*sigma^2)),0,1)^log2(lCode);
end

plot(SNR, succ_prob,'-sr')
hold on,
plot(SNR, succ_theory, '-ob')