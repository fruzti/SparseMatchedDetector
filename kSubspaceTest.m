%% Test Compressing Detection ( k - sparse )
clear all, close all

% dimension of subspace
dim_k = 2;

% length of code
lCode = 256;
numSubs = nchoosek(lCode, dim_k);
subsComb = combnk(1:lCode, dim_k);

% orthogonal codes
S = hadamard(lCode);
S = 1/norm(S(:,1)) * S;

% length of codeword <- reduced dimensionality
b = nextpow2(lCode);
% b = ;

% list of codewords
B = de2bi(0:lCode-1,b)';
B = B - ~B;
B_ext = de2bi(0:lCode-1,b)';
B_ext = B_ext - ~B_ext;

S_ext = zeros(lCode,numSubs);

for ii = 0:(numSubs-1)
    S_ext(subsComb(ii+1,1),ii+1) = 1;
    S_ext(subsComb(ii+1,2),ii+1) = 1;
end
%%
bNew = b;
lambda = 2;
subSubset = randperm(numSubs,lCode);

cvx_begin
    variable B_star(bNew,lCode)
    variable B_ext2(bNew,numSubs)
    minimize( norm( B_star*S_ext(:,1:lCode) - B_ext,'fro') )
cvx_end
close all, 

% lMax = min(max(svd(B_ext2)),max(svd(B_star)));
% B_ext2 = 10/lMax * B_ext2;
% B_star = 10/lMax * B_star;
%%
% random realisations
% nTest = 1e3;
nTest = 1;

% noise
% SNR = [-10:10:50];
SNR = inf;
lSNR = length(SNR);

% covariance matrix -> reduced dimensionality
C = B_ext*S_ext';
R0 = (C*C');

succ_prob = zeros(lSNR,1);
succ_theory = zeros(lSNR,1);
%%
for ss = 1:lSNR
    sigma = sqrt(10^(-SNR(ss)/10));
    R = sigma^2 * R0;
    
    fprintf('SNR: %d dB\n\n', SNR(ss))
    for nn = 1:nTest
    
        ind_n = randi([1,numSubs],1);
        x_n = sum(S(:,subsComb(ind_n,:)),2) + sigma * randn(lCode,1);
    
        b_tst = C*x_n*x_n';
    
        b_dec = ( b_tst > 0 );
        
        ind_tst = bi2de(b_dec') + 1;
    
        succ_prob(ss) = succ_prob(ss) + ( ind_tst == ind_n )/nTest;
    end
    succ_theory(ss) = cdf('Normal',sqrt(1/(lCode*sigma^2)),0,1)^log2(lCode);
end

plot(SNR, succ_prob,'-sr')
hold on,
plot(SNR, succ_theory, '-ob')